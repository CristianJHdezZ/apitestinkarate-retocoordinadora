Feature: Post API Solicitud de Recogida distintos escenarios de pruebas

  Background:
    * url baseURL
    * header Accept = 'application/json'
    * def bodyRequestInput = read("requestSolicitudRecogida.json")
    * def currentDate = Java.type('java.time.LocalDate').now()
    * def futureDate = currentDate.plusDays(5)
    * print 'Fecha actual:', currentDate
    * print 'Fecha futura en 5 dias: ', futureDate
    * def dataFaker = Java.type("com.github.javafaker.Faker")
    * def dataFakerObject = new dataFaker()
    * def valueEmpty = ""


  @SolicitudRecogidaFechaValida
  Scenario: Solicitud de Recogida Exitosa, ingresando una fecha futura dentro de los 5 días hábiles siguientes
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+futureDate
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * print 'Número aleatorio:', bodyRequestInput.documento
    When method post
    Then status 200
    * def expectedJson = { "isError": false,"data": { "id_recogida": { "idRecogida": "#ignore", "error": false, "message": "Solicitud recogida programada exitosamente"}},"timestamp": "#ignore","id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudInvalidaRecogidaExistenteConFechaInvalida
  Scenario: Solicitud de Recogida Existente, ingresando una fecha futura dentro de los 5 días hábiles siguientes pero Ya existe una recogida programada
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+futureDate
    When method post
    Then status 200
    * def expectedJson = {"isError":true,"data":{"message":"#regex .*Error, Ya existe una recogida programada para hoy, id:.*","idRecogidaAnterior":"#ignore","recogida_anterior":true},"timestamp":"#ignore","id":"#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFechaInvalida
  Scenario: Solicitud de Recogida fallida, ingresando una fecha futura fuera de los 5 días hábiles siguientes
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(10)
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"data": {"message": "#regex .*, no debe ser mayor a la fecha:.*","idRecogidaAnterior": "#regex .*, no debe ser mayor a la fecha.*","recogida_anterior": true},"timestamp": "#ignore","id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaNombreEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo nombreEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.nombreEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"nombreEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaApellidosEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo apellidosEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.apellidosEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"apellidosEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaCelularEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo celularEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.celularEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"celularEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaEmailUsuarioEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo emailUsuario es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.emailUsuario = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"emailUsuario\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaDescripcionTipoViaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo descripcionTipoVia es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.descripcionTipoVia = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"descripcionTipoVia\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaAplicativoEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo aplicativo es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.aplicativo = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"aplicativo\" must be one of [web, envios]", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response
