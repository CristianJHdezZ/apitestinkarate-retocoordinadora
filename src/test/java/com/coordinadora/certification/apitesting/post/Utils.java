package com.coordinadora.certification.apitesting.post;


import com.github.javafaker.Faker;

public class Utils{

    static Faker faker = new Faker();

    public static String generateRandomInteger(int size) {
        return faker.number().digits(size);
    }
}
