# APITestinKarate-RetoCoordinadora

## Requisitos

Asegúrate de tener instalados los siguientes componentes antes de ejecutar las pruebas:

- [Karate Framework](https://www.karatelabs.io/)  -  [Documentation GitHub](https://github.com/karatelabs/karate)
- [Gradle](https://gradle.org/)
- [Cucumber - Gherkin syntax](https://cucumber.io/docs/gherkin/reference/)


## Pasos de los Escenarios de pruebas

``` gherkin
@SolicitudRecogidaFechaValida
  Scenario: Solicitud de Recogida Exitosa, ingresando una fecha futura dentro de los 5 días hábiles siguientes
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+futureDate
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * print 'Número aleatorio:', bodyRequestInput.documento
    When method post
    Then status 200
    * def expectedJson = { "isError": false,"data": { "id_recogida": { "idRecogida": "#ignore", "error": false, "message": "Solicitud recogida programada exitosamente"}},"timestamp": "#ignore","id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudInvalidaRecogidaExistenteConFechaInvalida
  Scenario: Solicitud de Recogida Existente, ingresando una fecha futura dentro de los 5 días hábiles siguientes pero Ya existe una recogida programada
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+futureDate
    When method post
    Then status 200
    * def expectedJson = {"isError":true,"data":{"message":"#regex .*Error, Ya existe una recogida programada para hoy, id:.*","idRecogidaAnterior":"#ignore","recogida_anterior":true},"timestamp":"#ignore","id":"#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFechaInvalida
  Scenario: Solicitud de Recogida fallida, ingresando una fecha futura fuera de los 5 días hábiles siguientes
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(10)
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"data": {"message": "#regex .*, no debe ser mayor a la fecha:.*","idRecogidaAnterior": "#regex .*, no debe ser mayor a la fecha.*","recogida_anterior": true},"timestamp": "#ignore","id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaNombreEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo nombreEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.nombreEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"nombreEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaApellidosEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo apellidosEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.apellidosEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"apellidosEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaCelularEntregaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo celularEntrega es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.celularEntrega = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"celularEntrega\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaEmailUsuarioEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo emailUsuario es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.emailUsuario = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"emailUsuario\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaDescripcionTipoViaEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo descripcionTipoVia es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.descripcionTipoVia = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"descripcionTipoVia\" is not allowed to be empty", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

  @SolicitudRecogidaFallidaAplicativoEnBlanco
  Scenario: Solicitud de Recogida fallida, no se permitirá realizar la solicitud de recogida, el campo aplicativo es obligatorio
    Given request bodyRequestInput
    * set bodyRequestInput.fechaRecogida = ""+currentDate.plusDays(5)
    * set bodyRequestInput.documento = dataFakerObject.number().digits(10)
    * set bodyRequestInput.aplicativo = valueEmpty
    When method post
    Then status 200
    * def expectedJson = {"isError": true,"message": "Los valores de entrada no son correctos.", "code": "BAD_MESSAGE", "cause": "\"aplicativo\" must be one of [web, envios]", "timestap": "#ignore","statusCode": 200,"id": "#string"}
    And match response == expectedJson
    And print response

```

## Clone Project

```
cd existing_repo
git clone https://gitlab.com/CristianJHdezZ/apitestinkarate-retocoordinadora.git
```

## Run project in terminal

```
gradle clean test --tests "com.coordinadora.certification.apitesting.ManagementTest" -i
```
## Run project in Pipelines of gitlab 

```
https://gitlab.com/CristianJHdezZ/apitestinkarate-retocoordinadora/-/pipelines
```

![img.png](src/test/resources/image/img.png)
![img_1.png](src/test/resources/image/img_1.png)
![img_3.png](src/test/resources/image/img_3.png)
![img_4.png](src/test/resources/image/img_4.png)

## Validate evidence from executed test cases 

![img_6.png](src/test/resources/image/img_6.png)
![img_7.png](src/test/resources/image/img_7.png)
![img_8.png](src/test/resources/image/img_8.png)

https://gitlab.com/CristianJHdezZ/apitestinkarate-retocoordinadora/-/jobs/6759897063/artifacts/browse/build/
![img_9.png](src/test/resources/image/img_9.png)

Evidencies:
https://cristianjhdezz.gitlab.io/-/apitestinkarate-retocoordinadora/-/jobs/6759897063/artifacts/build/cucumber-html-reports/overview-features.html
![img_10.png](src/test/resources/image/img_10.png)

https://cristianjhdezz.gitlab.io/-/apitestinkarate-retocoordinadora/-/jobs/6759897063/artifacts/build/karate-reports/karate-summary.html
![img_11.png](src/test/resources/image/img_11.png)

![img_12.png](src/test/resources/image/img_12.png)
